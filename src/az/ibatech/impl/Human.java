package az.ibatech.impl;

import az.ibatech.HumanInter;

public class Human implements HumanInter {
    private String name;
    private String surname;
    private int dateOfBirth;
    private int iqLevel;
    private Pet pet;
    private Human mother;
    private Human father;
    private String[][] schedule;

    public Human() {
    }

    public Human(String name, String surname, int dateOfBirth) {
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
    }

    public Human(String name, String surname, int dateOfBirth, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.iqLevel = iqLevel;
        this.pet = pet;
        this.mother = mother;
        this.father = father;
    }

    public Human(String name, String surname, int dateOfBirth, int iqLevel, Pet pet, Human mother, Human father, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.iqLevel = iqLevel;
        this.pet = pet;
        this.mother = mother;
        this.father = father;
        this.schedule = schedule;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(int dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public int getIqLevel() {
        return iqLevel;
    }

    public void setIqLevel(int iqLevel) {
        if (iqLevel >= 1 && iqLevel <= 100)
            this.iqLevel = iqLevel;
        else
            System.out.println("Please enter the number from 1 to 100 ");
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    @Override
    public String toString() {
        return "Human{ " +
                "name = " + name + ' ' +
                " surname = " + surname + ' ' +
                " dateOfBirth = " + dateOfBirth + ' ' +
                " iqLevel = " + iqLevel + "\n" +
                " pet = " + pet + "\n" +
                " mother = " + mother.getName() + " " + mother.getSurname() + "\n" +
                " father = " + father.getName() + " " + father.getSurname();
    }

    @Override
    public void greetPet() {
        System.out.println("Hello," + pet.getNickname());
    }

    @Override
    public void describePet() {
        System.out.println("I have a " + pet.getSpecies() +
                ",\nHe is " + pet.getAge() + " years old" +
                ",\nhe is " + (pet.getTrickLevel() > 50 ? "very sly" : "almost not sly"));
    }

    @Override
    public boolean feedPet(int generateNum) {
        if (generateNum < pet.getTrickLevel()) {
            System.out.println("Hm... I will feed Jack's " + pet.getNickname());
            return true;
        } else {
            System.out.println("I think Jack is not hungry.");
            return false;
        }
    }
}
