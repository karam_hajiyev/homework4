import az.ibatech.impl.Human;
import az.ibatech.impl.Pet;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Random random = new Random();
        String[] habits = {"eat", "drink", "sleep"};
        String[][] schedule = {{"Sunday", "Walking"},
                {"Monday", "GYM"},
                {"Tuesday", "Swimming"},
                {"Wednesday", "Joking"},
                {"Thursday", "Sprinting"},
                {"Friday", "Boxing"},
                {"Saturday", "Karate"}
        };
        Pet myPet = new Pet("Dog", "Rock", 5, 75, habits);
        Human mother = new Human("Jane", "Karleone", 1957);
        Human father = new Human("Vito", "Karleone", 1960);
        Human human = new Human("Michael", "Karleone", 1977,
                90, myPet, mother, father, schedule);
        System.out.println(human);
        human.greetPet();
        human.describePet();
        human.feedPet(random.nextInt(100));
    }
}
