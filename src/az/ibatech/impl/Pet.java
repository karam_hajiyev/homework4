package az.ibatech.impl;

import az.ibatech.PetInter;

import java.util.Arrays;

public class Pet implements PetInter {
    private String species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    public Pet() {
    }

    public Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    public Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        if (trickLevel >= 1 && trickLevel <= 100)
            this.trickLevel = trickLevel;
        else
            System.out.println("Please enter the number from 1 to 100 ");
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    @Override
    public String toString() {
        return "Pet " +
                " species = " + species + ' ' +
                " nickname = " + nickname + ' ' +
                " age = " + age +
                " trickLevel = " + trickLevel + "\n" +
                " habits = " + Arrays.toString(habits);
    }

    @Override
    public void eat() {
        System.out.println("I am eating");
    }

    @Override
    public void respond(String petName) {
        System.out.println("Hello, owner. I am - " + petName + ". I miss you!");
    }

}
